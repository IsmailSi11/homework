#1. Sum of all digits of a number
def sum_or_digits(num):
    sum_result = 0

    while num % 10 :
        digit = num %10
        num = num // 10
        sum_result = sum_result + digit

    return sum_result
    
#print(sum_or_digits(8351)) 


#2. Turn a number into a list of digits
def to_digits(n):
    my_list = []
    while n > 0 :
        digit = n % 10
        my_list.append(digit)
        n = n // 10

    return my_list

#print (to_digits(8531))  

#3. Turn a list of digits into a number

def to_number(digits):
    num_sum = 0
    for digit in digits:
        if digit <=10:
            num_sum=num_sum * 10
        else:
            num_sum=num_sum *100
            num_sum=num_sum+digit
    return num_sum
#print(to_number([8, 5, 3, 1]))

#4. Fibonacci number
def fib_number(num):
    current_num = 1
    previous_num = 0
    fib_num_arr = [1]
    fib_nums_sum = 0

    for num in range(1,num):
        next_num=current_num+previous_num
        fib_num_arr.append(next_num)
        current_num=next_num
    for num in fib_num_arr:
        if num <=10:
            fib_nums_sum=fib_nums_sum*10
        else:
            fib_nums_sum=fib_nums_sum*100
        fib_nums_sum=fib_nums_sum+num
        return fib_nums_sum
#print(fib_number(5))

#5.Is Number Balanced?
def is_number_balanced(number):
    number_digits = []
    left_part = 0
    right_part = 0
    while number % 10 > 0:
        digit = number % 10
        number_digits.append(digit)
        number = number // 10
    number_digits.reverse()
    list_lenght = len(number_digits)
    middle = len(number_digits) // 2
    
    if len(number_digits) == 1:
        return True

    for index in range(list_lenght):
        if index < middle:
            left_part = left_part + number_digits[index]
        else:
            right_part = right_part + number_digits[index]
    return left_part == right_part

#print(is_number_balanced(9))
#print(is_number_balanced(8531))
#print(is_number_balanced(28471))


#6.Descreasing sequence?
def is_decreasing(list):
    for index in range(0, len(list) - 1):
        if list[index] <= list[index + 1]:
            return False
    return True


#print(is_decreasing([5, 4, 3, 2, 1]))
#print(is_decreasing([1, 1, 1, 1]))





#7.Sum the minimum and maximum elements

def sum_of_min_and_max(arr):
    max_number = arr[0]
    min_number = arr[0]



    for digit in arr:
        if max_number<digit:
            max_number=digit
        if min_number>digit:
            max_number=digit

    return max_number+min_number

#print(sum_of_min_and_max([8,3,5,1]))


#8.Sum all divisors of an integer


def sum_of_divisors(n):
    divisors_sum = 0
    for num in range(1, n + 1):
        if n % num == 0 :
            num += divisors_sum 
    return divisors_sum

#print(sum_of_divisors(6))
#print(sum_of_divisors(8))

#9.Check if integer is prime

def is_prime(n):
    if n <=1:
        return False
    for num in range(2,n):
        if n % num==0:
            return False
    return True
#print(is_prime(1))
#print(is_prime(2))
#print(is_prime(5))
#print(is_prime(-12))


#10.Integer Palindromes
def is_int_palidnrome(n):
    number_list=[]
    number_list_reverse=[]
    while n > 0:
        digit=n %10
        number_list.append(digit)
        number_list_reverse.append(digit)
        n=n//10
    number_list.reverse()
    return number_list==number_list_reverse


#print(is_int_palidnrome(1))
#print(is_int_palidnrome(42))
#print(is_int_palidnrome(8531))
#print(is_int_palidnrome(999))
#print(is_int_palidnrome(123))   

#11.Number containing a single digit?
def contains_digit(number, digit):
    digits_list=[]
    while number > 0:
        num_digit = number % 10
        digits_list.append(num_digit)
        number = number // 10
    digits_list.reverse()
    if digit in digits_list:
        return True
    return False

#print(contains_digit(123,4))
#print(contains_digit(1000,0))



#12.Counting substrings
def count_substrings(haystack, needle):
    kom_pi = list(haystack)
    kom_pi.sort()
    cucuk = list (needle)
    cucuk.sort()
    count = haystack.count(needle)

    return(count)

#print(count_substrings("This is a test string", "is"))
#print(count_substrings("babababa", "baba"))
#print(count_substrings("python is an awesome language to program in!", "o"))





#13
def biggest_difference(arr):
    if len(arr) == 1:
        return arr[0] - arr[0]

    min = arr[0] - arr[1]

    for idx1 in range(0, len(arr)):
    
        for idx2 in range(0,len(arr)):
            if arr[idx1] - arr [idx2] < min:
                min = arr[idx1] - arr[idx2]
        return min
#print(biggest_difference([1,2,3,4,5]))

#14
def slope_style_score(arr):
    min = arr[0]
    max = arr[0]
    scores = []

    for score in arr:
        if score > max:
            max = score
        if score < min:
            min = score
        if min == max:
            return round(min, 2)

        for idx in range(0, len(arr)):
            if arr[idx] != max:
                scores.append(arr[idx])

            if len(scores) == 0:    
                return round((min + max) / 2, 2)
            
            score = 0
            for idx in range(0, len(scores)):
                score = score + scores[idx]
            return round(score / len(scores), 2)

#print(slope_style_score([87,87,92,92,91]))

#15
def what_is_my_sign(day, month):
    signs = [ "Aquaris", "Pisces", "Aries", "Taurus", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagitarius","Capricorn"]
    if (month < 1):
        if month > 12:
            return "invalid month"
    if day < 1:
        if day > 31:
            return "invalid day"
    border = 19.5
    for idx in range(0,12):
        if idx == month - 1:
            if day < border:
                if month == 1:
                    return signs[11]
                else:
                    return signs[idx - 1]
            else:
                return signs[idx]
        if idx < 9:
            border = border + 0.5
        else:
            border = border - 1

#print(what_is_my_sign(27, 1))

#16
def count_coins(sum):
    coins = [1,0.5,0.2,0.1,0.05,0.02,0.01]
    dict = {100: 0,50: 0,20: 0,10: 0,5: 0,2: 0,1: 0}
    sum2 = 0.0
    for idx in range(0, len(coins)):
        while sum2 < sum:
            if sum2 + coins[idx] > sum:
                break
            else:
                sum2 = sum2 + coins[idx]
                dict[int(coins[idx] * 100)] = dict[int(coins[idx] * 100)] + 1
        
    return dict
#print(count_coins(0.09))



