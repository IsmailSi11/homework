class Player:
    def __init__(self, name, health, weapons):
        self.__name = name
        self.__health = health
        self.__weapons = weapons

    def get_name(self):
        return self.__name

    def set_name(self, a):
        self.__name = a

    def get_health(self):
        return self.__health

    def set_healt(self, b):
        self.__health = b

    def get_weapons(self):
        return self.__weapons

    def set_weapons(self, c):
        self.__weapons = c

    def take_damage(self, damage):
        self.__health -= damage

    def reload(self, Weapon):
        Weapon.reload()

    def shoot(self, Weapon,  enemy_player):
        enemy_player.take_damage(fire)


player1 = Player("Niki", 77, "Deagle")

print(player1.get_name())
print(player1.get_health())
print(player1.get_weapons())


class Weapon:
    def __init__(self, damage, clip_size, clip_bullets):
        self.damage = damage
        self.clip_size = clip_size
        self.clip_bullets = clip_bullets

    def reload(self):
        self.clip_bullets = self.clip_size

    def fire(self):
        if self.clip_bullets > 0:
            self.clip_bullets -= 1
            return self.damage
        else:
            return 0


wp = Weapon(100, 90, 30)



class ArmoredPlayer(Player):
    def __init__(self, name, health,weapons, armor):

        super().__init__(name, health, weapons)
        self.armor = armor
    def take_damage(self, damage):
        if self.armor >= damage:
            self.armor -= damage
            return damage
            
        return super().take_damage(damage)
                



print(wp.damage)
print(wp.clip_size)
print(wp.clip_bullets)
