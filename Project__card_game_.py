class Card:
    suits = ["spades",
             "hearts",
             "diamonds",
             "clubs"]

    values = [None, None,"2", "3",
              "4", "5", "6", "7",
              "8", "9", "10",
              "Jack", "Queen",
              "King", "Ace"]

    def __init__(self, value, suit):
        self.value = v
        self.suit = s

    def __lt__(self, c2):
        if self.value < c2.value:
            return True
        if self.value == c2.value:
            if self.suit < c2.suit:
                return True
            else:
                return False
        return False

    def __gt__(self, c2):
        if self.value > c2.value:
            return True
        if self.value == c2.value:
            if self.suit > c2.suit:
                return True
            else:
                return False
        return False

    def __repr__(self):
        v = self.values[self.value] +\
            " of " + \
            self.suits[self.suit]
        return v


class Deck:
    def __init__(self):
        self.cards = []
        for i in range(2, 15):
            for j in range(4):
                self.cards\
                    .append(Card(i, j))
        shuffle(self.cards)

    def rm_card(self):
        if len(self.cards) == 0:
            return
        return self.cards.pop()


class Player:
    def __init__(self, name):
        self.wins = 0
        self.card = None
        self.name = name


class Game:
    def __init__(self):
        name1 = input("Ismail")
        name2 = input("Mehmed")
        self.deck = Deck()
        self.Ismail = Player(name1)
        self.Mehmed = Player(name2)

    def wins(self, winner):
        w = "{} wins this round"
        w = w.format(winner)
        print(w)

    def draw(self, Ismail, Ismailc, Mehmed, Mehmedc):
        d = "{} drew {} {} drew {}"
        d = d.format(Ismail, Ismailc, Mehmed, Mehmedc)
        print(d)

    def play_game(self):
        cards = self.deck.cards
        print("Beginning War!")

        while len(cards) >= 2:
            m = "q to quit. Any " + \
                "key to play:"
            response = input(m)
            if response == 'q':
                break
            Ismailc = self.deck.rm_card()
            Mehmedc = self.deck.rm_card()
            Ismail = self.Ismail
            Mehmed = self.Mehmed
            self.draw(Ismail, Ismailc, Mehmed, Mehmedc)
            if Ismailc > Mehmedc:
                self.Ismail.wins += 1
                self.wins(self.Ismail)
            else:
                self.Mehmed.wins += 1
                self.wins(self.Mehmed)

        win = self.winner(self.Ismail, self.Mehmed)
        print("War is over.{} wins".format(win))

    def winner(self, Ismail, Mehmed):
        if Ismail.wins > Mehmed.wins:
            return Ismail
        if Ismail.wins < Mehmed.wins:
            return Mehmed
        return "It was a easy!"
