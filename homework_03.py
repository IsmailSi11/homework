def sum_of_numbers(st):
    substitute_st = "0"
    num_sum = 0

    for i in st:
        if i.isdigit():
            substitute_st += i
        else:
            num_sum += int(substitute_st)
            substitute_st = "0"
    return num_sum + int(substitute_st)


print(sum_of_numbers("ab12315cd3"))
print(sum_of_numbers("ab12df55ads"))
print(sum_of_numbers("ab"))






def is_anagram(fst_word, snd_word):
    fst_word = fst_word.lower()
    snd_word = snd_word.lower()
    fst_word_lenght = len(fst_word)
    snd_word_lenght = len(snd_word)

    if fst_word_lenght == snd_word_lenght:
        for item in range(fst_word_lenght):
            for item2 in range(snd_word_lenght):
                if fst_word[item] == snd_word[item2]:
                    return True
    return False


print(is_anagram("BRADE", "BeaRD"))
print(is_anagram("TOP_CODER", "COTO_PRODE"))
print(is_anagram("Tar", "Rat"))
print(is_anagram("Arc", "Car"))
print(is_anagram("test", "treserio"))




def count_words(arr):
    counted_words = {}
    for item in arr:
        counter = 0
        for same_item in arr:
            if item == same_item:
                counter += 1
        counted_words[item] = counter
    return counted_words


print(count_words(["apple", "banana", "apple", "pie", "banana"]))


